const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const morgan = require('morgan');
const path = require("path");

const app = express();

const accessLogStream = fs.createWriteStream(path.join(__dirname, 'logger.log'), { flags: 'a' })
app.use(morgan('combined', { stream: accessLogStream }))

const filesExtension = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

const createFile = (body,res) => {
    const extension = body.filename.split('.')[body.filename.length-1];

    if (!filesExtension.some((el) => el === extension)) {
        res.status(400).json({ message: 'Wrong file extension' })
        return false;
    }
    const content = body.content;
    fs.writeFile(`${__dirname}/files/${body.filename}`, content, err => {
        if (err) {
            return false;
        }
    })
    return true;
}

const onPost = (req, res) => {
    if(createFile(req.body, res)){
        res.status(200).json({message: 'success'});
    } else {
        res.status(400).json({});
    }
}

const onGetFileList = (req,res) => {
    const filePath = path.join(__dirname, 'files')

    if (fs.existsSync(filePath)) {
        fs.readdir(filePath, (err, files) => {
            if (err) {
                console.log(err);
                res.status(400).json({ message: err })
            }
            res.status(200).json({ message: 'success', files })
            if (files.length > 0) {
                files.forEach(file => {
                    console.log(file);
                });
            }
        });
    } else {
        res.status(500).json({ message: 'Wrong search directory' });
        throw new Error('Wrong search directory');
    }

}

const onGetFile = (req,res) => {
    const filename = req.params.filename;
    const filePath = path.join(__dirname, 'files', filename);

    if (fs.existsSync(filePath)) {
        const filesExtension = req.params.filename.match(/.log$|.txt$|.json$|.yaml$|.xml$|.js$/i)[0].split('.')[1];
        fs.readFile(filePath, 'utf8', (err, content) => {
            if (err) {
                return
            }
            res.status(200).json({ message: 'Success', filename, content, filesExtension, uploadedDate: Date.now()});
        });
    } else {
        res.status(400).json({message: "File wasnt found"});
        throw new Error('File wasnt found');
    }
}

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.get('/api/files', onGetFileList);
app.get('/api/files/:filename',onGetFile);
app.post('/api/files', onPost);

app.listen(8080,()=>{
    console.log('start');
});